
---

# What is aws? 

- AWS stands for amazon web services
- Cloud provider
  - Cloud ?
- On-demend resource provision
- Pay as you go
- Network accessible

---

# Shared responsibility model

AWS and Customer share responsibility of running and maintaining Cloud infrastructure
- Who shares responsibility ?
  - AWS responsibility:
    - Security **Of** The Cloud
  - Client responsibility:
    - Security **In** The Cloud
    - Data **In** The Cloud is owned by Customer and will be owned by customer, unless explicitly shared by the Customer to all.
- Who Own IT Controls ?
  - AWS:
    - Physical Controls
    - Environment Controls
  - Middle-Ground of AWS and Customer sharing controls:
    - Patch Management
    - Configuration Management
    - Awareness and Training
  - Customer responsibility
    - Region Choices
    - Service/features


---

# Aws account



---

# Demo: aws console

---

# Service categories

---

# Aws icons and diagrams

