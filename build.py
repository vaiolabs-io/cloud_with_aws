#!/usr/bin/env python3

import os
import sys

try:
    import pdfkit
    from markdown import markdown
except ImportError:
    print('[!] Missing Library')


def main():
    chapters = []
    md_chapters = ''
    if not os.path.exists('./out'):
        os.mkdir('./out')
    for path, dirs, files in os.walk('.',topdown=True):
        if 'README.md' in files and './.git' not in files:
            chapters.append(str(path)+'/'+files[0])
    chapters.sort()
    for chapter in chapters:
        with open(chapter, 'r') as md_file:
            md = md_file.read()
        md_chapters += md
    html_content = markdown(md_chapters)

    with open('./out/chapters.html', 'w') as html_file:
        html_file.write(html_content)



if __name__ == '__main__':
    main()
